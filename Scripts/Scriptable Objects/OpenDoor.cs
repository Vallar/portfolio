﻿using UnityEngine;
using System.Collections;

public class OpenDoor : MonoBehaviour
{
    [SerializeField] private bool isOpen = false;
    private Collider2D col;

    private void Awake()
    {
        col = GetComponent<Collider2D>();
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Player"))
        {
            if (isOpen)
            {
                isOpen = true;
                col.enabled = false;
                //TODO: Add some animations in here.
            }
        }

    }
}
