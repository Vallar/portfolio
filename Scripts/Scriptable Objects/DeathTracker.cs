﻿using UnityEngine;
using System.Collections;
using MEC;

public enum DeathReason { HOPE_INSUFFICIENT, FELL_IN_HOLE, HOPE_INSUFFICIENT_LEVEL_2}

[CreateAssetMenu(menuName = "Tracking/Death")]
public class DeathTracker : ScriptableObject
{
    public DeathInfo[] info;

    [System.Serializable]
    public struct DeathInfo
    {
        public DeathReason reason;
        public int count;
        public int upgradeThreshold;
        public bool upgradeReceived;
        public Upgrades upgrade;
    }

    public void UpdateDeathReason(DeathReason _reason)
    {
        for (int i = 0; i < info.Length; i++)
        {
            if (info[i].reason == _reason)
            {
                info[i].count += 1;
                return;
            }
        }
    }

    public void CheckUpgrades(PlayerStats _stats)
    {
        for (int i = 0; i < info.Length; i++)
        {
            if (info[i].count > info[i].upgradeThreshold && info[i].upgradeReceived == false)
            {
                info[i].upgrade.ActivateUpgrade(_stats);
                info[i].upgradeReceived = true;
                GUIManager.instance.ShowUpgradeReceived(info[i].upgrade.description, info[i].upgrade.icon);
                return;
            }
        }
    }
}
