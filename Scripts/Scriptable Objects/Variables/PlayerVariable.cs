﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Variables/Player")]
public class PlayerVariable : ScriptableObject
{
    public Transform playerTransform;
    public PlayerStats stats;

    private void OnDisable()
    {
        playerTransform = null;
        stats = null;
    }
}
