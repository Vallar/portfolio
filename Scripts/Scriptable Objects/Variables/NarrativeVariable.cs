﻿using UnityEngine;

[CreateAssetMenu(menuName = "Variables/Narrative Piece")]
public class NarrativeVariable : ScriptableObject
{
    [TextArea]
    public string text;
}
