﻿using UnityEngine;
using System.Collections;


public class Upgrades : ScriptableObject
{
    public float upgradeAmount;
    public string description;
    public Sprite icon;

    public virtual void ActivateUpgrade(PlayerStats stats)
    {
        
    }
}
