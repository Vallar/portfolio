﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName ="Upgrades/Speed")]
public class SpeedUpgrade : Upgrades
{
    public override void ActivateUpgrade(PlayerStats stats)
    {
        stats.speed += upgradeAmount;
    }
}
