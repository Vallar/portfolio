﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Upgrades/Lose Less Hope")]
public class LoseLessHopeUpgrade : Upgrades
{
    public override void ActivateUpgrade(PlayerStats stats)
    {
        //stats.hopeReductionTimer.timeInterval += upgradeAmount;
    }
}
