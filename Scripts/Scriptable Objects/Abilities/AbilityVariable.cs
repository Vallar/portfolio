﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MEC;

[CreateAssetMenu(menuName = "Variables/AbilityVariable")]
public class AbilityVariable : ScriptableObject
{
    [HideInInspector] public bool isActive;
    [HideInInspector] public bool isCooldown;
    [SerializeField] private float timeTillStop;
    [SerializeField] private float cooldownTimer;
    [SerializeField] private bool originalValue;

    private void OnEnable()
    {
        isActive = originalValue;
        isCooldown = originalValue;
    }

    public IEnumerator<float> StartAbility()
    {
        isActive = true;

        yield return Timing.WaitForSeconds(timeTillStop);

        isActive = false;
    }

    public IEnumerator<float> StartCooldown()
    {
        isCooldown = true;

        yield return Timing.WaitForSeconds(cooldownTimer);

        isCooldown = false;
    }

}
