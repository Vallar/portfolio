﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MEC;

[CreateAssetMenu ( menuName = "Effects/Flicker")]
public class Flicker : ScriptableObject
{
    public SpriteRenderer sr;
    public PlayerStats stats;
    public float timeBetweenFlickers;
    public float flickerDuration;

    public IEnumerator<float> StartFlicker()
    {
        for (int i = 0; i < GameManager.instance.waitBeforeDeathRoutine; i++)
        {
            sr.color = Color.red;

            yield return Timing.WaitForSeconds(0.25f);

            if (stats.currentHope > 0)
            {
                sr.color = Color.white;
                GameManager.instance.ResetDeathRoutine();
                yield break;
            }

            sr.color = Color.white;

            yield return Timing.WaitForSeconds(0.25f);
        }
    }
}
