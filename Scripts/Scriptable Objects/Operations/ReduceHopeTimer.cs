﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Operations/Reduce Hope")]
public class ReduceHopeTimer : ScriptableObject
{
    public int hopeLost = 1;
    public float timeInterval = 2;

    public IEnumerator<float> ReduceHope(PlayerStats _stats)
    {
        yield return Timing.WaitForSeconds(timeInterval);

        _stats.currentHope -= hopeLost;
        GUIManager.instance.UpdateHopeUI(_stats.currentHope);

        if (_stats.currentHope == 0 && GameManager.instance.activatedDeathTimer == false)
            yield return Timing.WaitUntilDone(Timing.RunCoroutine(GameManager.instance.DeathRoutine(DeathReason.HOPE_INSUFFICIENT), "gameOver"));
        else if (_stats.currentHope > 0)
            Timing.RunCoroutine(ReduceHope(_stats), "reduceHope");
    }
   
}
