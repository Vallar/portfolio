﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Operations/Reduce Value(0)")]
public class ReduceValue : ScriptableObject
{
    public int ReduceValuePassed(int _currentValue, int _amount)
    {
        if (_currentValue - _amount > 0)
            return _currentValue -= _amount;
        else
            return 0;
    }
}
