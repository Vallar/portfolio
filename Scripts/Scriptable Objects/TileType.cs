﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "Map/Create Tile")]
public class TileType : ScriptableObject
{
    public string typeName;
    public GameObject[] typePrefabs;

    public GameObject ReturnTypePrefab()
    {
        int random = Random.Range(0, typePrefabs.Length);
        return typePrefabs[random];
    }
}
