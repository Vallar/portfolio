﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine.Events;

public class PlayerStats : MonoBehaviour
{
    public PlayerAbilities abilities;
    public int currentHope = 10;
    public float speed = 2;
    public bool canBreakWall = false;
    public Flicker flicker;
    //public ReduceHopeTimer hopeReductionTimer;

    [SerializeField] private ReduceValue reduceValue;
    private SpriteRenderer sr;

    private void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        abilities = GetComponent<PlayerAbilities>();
        flicker.sr = sr;
        flicker.stats = this;
    }


    //private void Start()
    //{
    //    ReduceHope();
    //}

    //public void ReduceHope()
    //{
    //    Timing.RunCoroutine(hopeReductionTimer.ReduceHope(this), "reduceHope");
    //}

    public void Die()
    {
        //TODO: Implement bells and whistels
        gameObject.SetActive(false);
    }
}
