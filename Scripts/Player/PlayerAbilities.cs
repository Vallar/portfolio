﻿using UnityEngine;
using System.Collections;

public class PlayerAbilities : MonoBehaviour
{
    public Abilities[] currentAbilities;
    public AbilityVariable isShielded;
    public AbilityVariable isJumping;
    public AbilityVariable isDoubleHope;
}
