﻿using UnityEngine;
using System.Collections;

public class PlayerMovement : MonoBehaviour
{
    //[SerializeField] private float forwardSpeed = 2;
    //[SerializeField] private float sidewaysSpeed = 5;
    //private Transform t;
    private Rigidbody2D rb;
    private PlayerStats stats;

    private void Awake()
    {
         //t = transform;
        stats = GetComponent<PlayerStats>();
        rb = GetComponent<Rigidbody2D>();
    }

    public void Move(float _x , float _y)
    {
        //t.Translate(new Vector2(_x * stats.speed, _y * stats.speed) * Time.deltaTime); 
        rb.MovePosition(Vector2.Lerp(rb.position, rb.position + (new Vector2(_x, _y) * stats.speed * Time.deltaTime), 0.15f));
    }
}
