﻿using UnityEngine;
using System.Collections;
using MEC;

public class PlayerCollision : MonoBehaviour
{
    //[SerializeField] private LayerMask layerMask;
    //private Transform t;
    //private float raycastLength;
    private PlayerStats stats;
    private bool firstTimeHope = true;

    private void Awake()
    {
        //t = transform;
        stats = GetComponent<PlayerStats>();
        //raycastLength = GetComponent<SpriteRenderer>().sprite.bounds.size.x + 0.1f;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag("Hope"))
        {
            stats.currentHope += 1;
            GUIManager.instance.UpdateHopeUI(stats.currentHope);
            collision.gameObject.SetActive(false);
            //TODO: Some bells and whistles

            if (firstTimeHope)
            {
                //Timing.RunCoroutine(GUIManager.instance.ShowStory(2));
                firstTimeHope = false;
            }

        }
    }
}
